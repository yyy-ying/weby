## git 的优点
- 适合多分支，分布式开发
- 开发灵活迅速
- 对服务器的压力比较小

## git的相关操作
- git init 新建一个git项目
- git clone git仓库的url  把远程仓库的内容克隆到本地
- git config user.name 设置用户名
- git config user.email 设置邮箱
- 如果需要全局设置，只需要在 config后面添加一个 --global，说明设置的是全局 git config --global user.name 用户名
- git add 文件名 把文件中所有修改的内容添加到缓存区域
- git add . 把项目中所有修改的文件内容都添加到缓存区域
- git commit -m '提交文件的信息'  把本地修改的内容从缓存区提交到文件库中
- git commit -am '提交文件的信息' 他是 git add 和 git commit 的合写
- git push 把修改的内容提交到远程仓库，如果不设置具体分支，则默认分支名为master （该操作也叫推送）
- git push origin 分支名 把更新内容提交到指定的分支
- git pull 把远程仓库上的代码拉取到本地

# 分支操作
- git branch 分支名 ： 创建一个分支
- git branch -a : 查看当前仓库的所有分支，前面有*代表的是当前操作的分支
- git checkout 分支名： 把当前操作的分支设置为 分支名
- git checkout -D 分支名 ：创建一个分支，并且把当前操作的分支设置为该分支
- git branch -d 分支名 删除本地分支
- git push origin --delete 分支名 删除远程分支
- git merge 分支名（aa）把分支aa合并到当前操作的分支上
### 分支变基
- 假如有两个分支A和B 要合并分支，变基的含义就是 将分支A提交内容在分支B上重新提交一遍，好处就是避免多个分支之间的冲突
- git rebase A B 它的含义是将B分支提交提取出来，在A分支上重新提交，使用的时候B可以省略，如果B省略它的含义是将当前操作的分支提交给分支A
## 代码标识
- A：新增的文件
- C：文件的一个新拷贝
- D：删除的文件
- M：文件被修改了
- R：文件名被修改了
- T: 文件的类型被修改了
- U: 文件没有被合并，需要合并后才能提交
- X：未知状态